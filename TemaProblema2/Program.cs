﻿using System;

namespace TemaProblema2
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabile de baza
            Console.WriteLine("Introduceti n");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            //ciclu pentru preluarea valorilor pentru vectorul nostru
            for(int i = 0; i < v.Length; i++)
            {
                v[i] = int.Parse(Console.ReadLine());
            }
            //alte variabile de control
            Console.WriteLine("Introduceti a");
            int a=int.Parse(Console.ReadLine());
            int count = 0;
            //ciclu de control
            for(int i = 0; i < v.Length; i++)
            {
                //statement de control cu un contor
                if (v[i] < a)
                {
                    count++;
                    Console.WriteLine(v[i] + " " + "este mai mic decat " + " " + a);
                }
            }
            //afisez numarul de var care sunt mai mici decat a
            Console.WriteLine("In total au fost " + " " + count + " " + "numere mai mici decat" + " " + a);
        }
    }
}
